package com.example.employee_manager.controllers;

import com.example.employee_manager.models.Employee;
import com.example.employee_manager.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    // trang index.jsp,  dsnhanvien.jsp
    @GetMapping(value = {"/","dsnhanvien"})
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("dsnhanvien");
        List<Employee> employeeList = employeeService.getAllEmployee();
        modelAndView.addObject("employees",employeeList);
        return modelAndView;
    }

    // Trang chitietnhanvien.jsp
    @GetMapping("/employee/{id}")
    public ModelAndView employeeDetails(@PathVariable String id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("chitietnhanvien");
        Optional<Employee> employeeOptional = employeeService.getEmployeeById(id);
        if(employeeOptional.isPresent()){
            modelAndView.addObject("employee",employeeOptional.get());
        }
        return modelAndView;
    }

    // Trang themnhanvien.jsp
    @GetMapping("/themnhanvien")
    public String newEmployee(Model model){
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "themnhanvien";
    }

    // Trang suanhanvien.jsp
    @GetMapping("/suanhanvien/{id}")
    public String editStudent(Model model, @PathVariable String id) {
        Optional<Employee> employeeOptional = employeeService.getEmployeeById(id);
        if (employeeOptional.isPresent())
            model.addAttribute("employee", employeeOptional.get());
        return "suanhanvien";
    }

    // Validate khi thêm nhân viên
    @PostMapping("/api/employees")
    public String addEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult bindingResult){
        /*lưu trữ kết quả kiểm tra hợp lệ (validation) của dữ liệu đầu vào sau khi được binding (liên kết) với đối tượng ModelAttribute. */
        if(bindingResult.hasErrors()){
            // Nếu có lỗi thì return về tràng thêm nhân viên
            System.out.println("error");
            return "themnhanvien";
        } else {
            String result = employeeService.saveEmployee(employee);
            System.out.println("Thêm thành công"+result);
            return "redirect:/";
        }
    }

    // Validate khi chỉnh sửa Nhân viên
    @PostMapping("/api/employee/{id}")
    public String updateEmployee(
            @Valid @ModelAttribute("employee") Employee employee,
            Employee newEmployee,
            @PathVariable String id,
            BindingResult bindingResult
                                ) {
        if(bindingResult.hasErrors()) {
            System.out.println("error");
            return "suanhanvien";
        } else {
            String result = employeeService.updateEmployee(id,newEmployee);
            System.out.println(result);
            return "redirect:/";
        }

    }
}
