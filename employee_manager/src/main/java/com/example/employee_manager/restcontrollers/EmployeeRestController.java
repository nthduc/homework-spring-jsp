package com.example.employee_manager.restcontrollers;

import com.example.employee_manager.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {

    @Autowired
    EmployeeService employeeService;

    // Kiểm tra nếu Employee tồn tại -> api/employeeExist/{id}
    @GetMapping("/employeeExist/{id}")
    public String isExistStudent(@PathVariable String id){
        String result = employeeService.isExistId(id);
        System.out.println(result);
        return result;
    }

    // Xóa Employee từ Databse
    @DeleteMapping("/employee/{id}")
    public String deleteEmployee(@PathVariable String id){
        String result = employeeService.deleteEmployee(id);
        System.out.println(result);
        return result;
    }


}
