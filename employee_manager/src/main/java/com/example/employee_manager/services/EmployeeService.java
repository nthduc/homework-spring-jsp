package com.example.employee_manager.services;

import com.example.employee_manager.models.Employee;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface EmployeeService {
    List<Employee> getAllEmployee();
    Optional<Employee> getEmployeeById(String id);
    String isExistId(String id);
    String saveEmployee(Employee employee);
    String updateEmployee(String id, Employee newEmployee);
    String deleteEmployee(String id);
}
