package com.example.employee_manager.services;

import com.example.employee_manager.models.Employee;
import com.example.employee_manager.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService{
    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> getEmployeeById(String id) {
        return employeeRepository.findById(id);
    }

    @Override
    public String isExistId(String id) {
        boolean isExist = employeeRepository.existsById(id);
        if(isExist) return "Id đã tồn tại";
        else return "Id hợp lệ";
    }

    @Override
    public String saveEmployee(Employee employee) {
        boolean isExistEmployee = employeeRepository.existsById(employee.getId());
        if (isExistEmployee) return "Id đã tồn tại !";
        else {
            employeeRepository.save(employee);
            return "Thêm Employee thành công !";
        }
    }

    @Override
    public String updateEmployee(String id, Employee newEmployee) {
        Employee result = getEmployeeById(id).map(employee -> {
            employee.setName(newEmployee.getName());
            employee.setMale(newEmployee.isMale());
            employee.setBirthday(newEmployee.getBirthday());
            employee.setAddress(newEmployee.getAddress());
            employee.setPhone(newEmployee.getPhone());
            employee.setDepartment(newEmployee.getDepartment());
            employee.setTitle(newEmployee.getTitle());
            employee.setWageRate(newEmployee.getWageRate());
            return employeeRepository.save(employee);
        }).orElseGet(()->  null
        );
        if(result != null) return "Edit thành công !";
        else return "Edit không thành công !";
    }

    @Override
    public String deleteEmployee(String id) {
       employeeRepository.deleteById(id);
       return "Xóa thành công id:" + id;
    }
}
