package com.example.employee_manager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Employee {
    @Id
    @NotBlank(message = "Vui lòng nhập id!")
    private String id;

    private String name;
    private boolean male;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date birthday;
    private String address;
    private String phone;
    private String department;
    private String title;
    private Long wageRate;


    public Employee(String id, String name, boolean male, String birthday, String address, String phone, String department, String title, Long wageRate) {
        this.id = id;
        this.name = name;
        this.male = male;
        this.birthday = parseStringToDate(birthday);
        this.address = address;
        this.phone = phone;
        this.department = department;
        this.title = title;
        this.wageRate = wageRate;
    }

    public String getBirthday() {
        if(this.birthday==null) return "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dateFormat.format(this.birthday);
        return strDate;
    }

    public void setBirthday(String birthday) {
        this.birthday = parseStringToDate(birthday);
    }

    public Date parseStringToDate(String s) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = format.parse(s);
            return date;
        }
        catch (ParseException e){
            throw new RuntimeException("Vui lòng nhập lại theo định dạng ngày!");
        }
    }

}
