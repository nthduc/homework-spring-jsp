package com.example.employee_manager.db;
import com.example.employee_manager.models.Employee;
import com.example.employee_manager.repositories.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Database {

    @Bean
    CommandLineRunner initDatabase (EmployeeRepository employeeRepository){
        return args -> {
            Employee s1 = new Employee("A001","Trần Văn Trà",false,"11/11/1954","Hà Nội", "12345678910", "BGĐ","GĐ",1000000l);
            employeeRepository.save(s1);
            Employee s2 = new Employee("A002","Nguyễn thị Mỹ Dung",true,"1/1/1975","TPHCM","10987654321", "KT", "NV",5000l);
            employeeRepository.save(s2);
            Employee s3 = new Employee("A003","Trần Quyết Thắng",false,"14/08/1974","Đà Nẵng","13579246810", "KHTV", "PP",70000l);
            employeeRepository.save(s3);
            Employee s4 = new Employee("B004","Phạm Hùng",false,"25/02/1977","Hải Phòng", "2468103579","SX", "TP",90000l);
            employeeRepository.save(s4);
        };
    }
}
