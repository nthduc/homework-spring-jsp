<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.6.0.min.js"></script>

        <title>Thêm nhân viên</title>
    </head>
    <body>
    <div class="new-employee">
        <h1>Thêm nhân viên</h1>
        <form:form method="POST" action="/api/employees" modelAttribute="employee">
            <table>
                <tr>
                    <td>
                        <form:label path="id" class="table-title" for="id">*Mã NV:</form:label>
                    </td>
                    <td>
                        <form:input path="id" style="width: 150px;" type="text" id="id" onkeyup="handleId()" />

                        <form:errors path="id" cssClass="error"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="name" class="table-title" for="name">*Họ tên:</form:label>
                    </td>
                    <td>
                        <form:input path="name" style="width: 200px;" type="text" id="name" />
                        <form:errors path="name" cssClass="error"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="male" class="table-title" for="gender">Nữ</form:label>
                    </td>
                    <td>
                        <form:checkbox  path="male" value="true" id="gender" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="birthday" class="table-title" for="birthday">*Ngày sinh:</form:label>
                    </td>
                    <td>
                        <form:input path="birthday" style="width: 175px;"  type="text" id="birthday" />
                        <span>dd/mm/yyyy</span>
                        <form:errors path="birthday" cssClass="error"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <form:label path="address" class="table-title" for="address">*Địa chỉ</form:label>
                    </td>
                    <td>
                        <form:input path="address" style="width: 175px;"  type="text" id="address" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="phone" class="table-title" for="phone">Số ĐT</form:label>
                    </td>
                    <td>
                        <form:input path="phone" style="width: 175px;"  type="tel" id="phone" />
                    </td>
                </tr>


                <tr>
                    <td>
                        <form:label path="department" class="table-title" for="department">Phòng ban</form:label>
                    </td>
                    <td>
                        <form:select path="department" id="department">
                            <form:option value="SX"/>
                            <form:option value="KT"/>
                            <form:option value="KHTV"/>
                            <form:option value="BGĐ"/>
                        </form:select>

                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="title" class="table-title" for="title">Chức vụ</form:label>
                    </td>
                    <td>
                        <form:select path="title" id="title">
                            <form:option value="GĐ"/>
                            <form:option value="PGĐ"/>
                            <form:option value="TP"/>
                            <form:option value="PP"/>
                            <form:option value="NV"/>
                        </form:select>

                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="wageRate" class="table-title" for="wageRate">Lương căn bản</form:label>
                    </td>
                    <td>
                        <form:input path="wageRate" style="width: 175px;"  type="text" id="wageRate" />
                    </td>
                </tr>
            </table>
            <div class="action">
                <input type="submit" value="Lưu" />
                <button>
                    <a href="/" style="text-decoration: none;color: black">
                        Hủy bỏ
                    </a>

                </button>
            </div>
        </form:form>

    </div>
    <script>
        function handleId(){
            const id = $('#id').val();
            const url = 'http://localhost:8080/api/employeeExist/' +id;
            console.log(url)
            $.ajax(
                url, {
                    success: function (result){
                        $('.messageId').text(result)
                    }
                }
            )
        }


    </script>
    </body>
</html>
