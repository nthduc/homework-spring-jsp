<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.6.0.min.js"></script>
        <title>Employee Manager</title>
    </head>

    <body>
    <div class="employee-list">
        <h1>DANH SÁCH NHÂN VIÊN</h1>
        <div>
            <h3 style="background-color: darkcyan;margin-bottom: 4px;">Employee List (${employees.size()})</h3>
            <div>
                <button>
                    <a href="/themnhanvien" style="text-decoration: none;color: black">
                        Thêm nhân viên
                    </a>

                </button>
                <button class="button-delete" onclick="handleClickDelete()" disabled>Xóa</button>

            </div>
        </div>
        <div>
            <table>
                <tr style="background-color: cyan;">
                    <th>Sửa</th>
                    <th>Chọn</th>
                    <th style="width: 25%;">Mã NV</th>
                    <th style="width: 40%;">Họ Tên</th>
                    <th style="width: 30%;">Ngày sinh</th>
                </tr>
                <c:forEach var="employee" items="${employees}">
                    <tr>
                        <td>
                            <button>
                                <a href="http://localhost:8080/suanhanvien/${employee.id}" style="text-decoration: none;color: black">Sửa
                                </a>
                            </button>
                        </td>
                        <td style="text-align:center">
                            <input type="checkbox" id-employee="${employee.id}" onchange="handleChangeCheckbox()">
                        </td>
                        <td>
                            <a href="/employee/${employee.id}">${employee.id}</a>
                        </td>
                        <td>${employee.name}</td>
                        <td>${employee.birthday}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <script>
        function handleChangeCheckbox(){
            const checkbox = document.querySelectorAll('input:checked');
            const btnDelete = document.querySelector('.button-delete');
            if(checkbox.length===0){
                btnDelete.disabled = true;
            }else {
                btnDelete.disabled = false;
            }
        }
        function handleClickDelete(){
            const checkboxes = document.querySelectorAll('input:checked');
            for(const checkbox of checkboxes){
                const id = checkbox.getAttribute("id-employee");
                handleDeleteStudent(id);
            }
        }

        function handleDeleteStudent(id){
            const url = 'http://localhost:8080/api/employee/' +id;
            $.ajax(
                url, {
                    method:"DELETE",
                    success: function (result){
                        alert(result)
                    }
                }
            )
        }




    </script>
    </body>

</html>
