<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta httdiv-equiv="X-UA-Comdivatible" content="IE=edge">
        <meta name="viewdivort" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <title>Chi tiết nhân viên</title>
    </head>

    <body>
    <div class="employee-detail">
        <h1>Chi tiết nhân viên</h1>
        <table>
            <tr>
                <th>Mã NV</th>
                <td>${employee.id}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>${employee.name}</td>
            </tr>
            <tr>
                <th>Nữ</th>
                <td>
                    <c:if test="${employee.male}">
                        <input type="checkbox" checked>
                    </c:if>
                    <c:if test="${employee.male==false}">
                        <input type="checkbox">
                    </c:if>
                </td>
            </tr>
            <tr>
                <th>Birthday</th>
                <td>${employee.birthday}</td>
            </tr>
            <tr>
                <th>Địa chỉ</th>
                <td>${employee.address}</td>
            </tr>
            <tr>
                <th>Số ĐT</th>
                <td>${employee.phone}</td>
            </tr>
            <tr>
                <th>Department</th>
                <td>${employee.department}</td>
            </tr>
            <tr>
                <th>Chức vụ</th>
                <td>${employee.title}</td>
            </tr>
            <tr>
                <th>Lương căn bản</th>
                <td>${employee.wageRate}</td>
            </tr>
            <tr>
                <td>
                    <button style="padding: 0 16px;">
                        <a href="http://localhost:8080/" style="text-decoration: none;color: black">Danh sách nhân viên
                        </a>
                    </button>
                </td>
                <td>
                    <button>
                        <a href="http://localhost:8080/suanhanvien/${employee.id}" style="text-decoration: none;color: black">Sửa
                        </a>
                    </button>
                </td>
            </tr>
        </table>
    </div>
    </body>

</html>
