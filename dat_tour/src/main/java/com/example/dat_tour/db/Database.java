//package com.example.dat_tour.db;
//
//import com.example.dat_tour.models.Booking;
//import com.example.dat_tour.models.Customer;
//import com.example.dat_tour.models.Tour;
//import com.example.dat_tour.repositories.BookingRepository;
//import com.example.dat_tour.repositories.CustomerRepository;
//import com.example.dat_tour.repositories.TourRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.Date;
//
//@Configuration
//public class Database {
//
//    @Bean
//    CommandLineRunner initDatabase (CustomerRepository customerRepository, TourRepository tourRepository, BookingRepository bookingRepository){
//        return args -> {
//            Customer c1 = new Customer(1l,"Lê Phi Hùng","TP.HCM","lephihung@email.com","12345678910");
//            Customer c2 = new Customer(2l,"Nguyễn Đức ","TPHCM","nguyenduc@gmail.com","12345678910");
//            customerRepository.save(c1);
//            customerRepository.save(c2);
//
//            Tour t1 = new Tour(1l,"Tham Quan núi","3","Xe Buýt","12/12/2020",2000);
//            Tour t2 = new Tour(2l,"Phú Quốc","3","Máy Bay","12/11/2020",5000);
//            tourRepository.save(t1);
//            tourRepository.save(t2);
//
//            Booking b1 = new Booking(1l,c1,new Date("12/12/2020"),2,1,t1);
//            Booking b2 = new Booking(2l,c2,new Date("12/11/2021"),4,1,t2);
//            Booking b3 = new Booking(2l,c2,new Date("12/08/2021"),6,10,t1);
//            Booking b4 = new Booking(2l,c1,new Date("12/07/2021"),2,3,t2);
//            bookingRepository.save(b1);
//            bookingRepository.save(b2);
//            bookingRepository.save(b3);
//            bookingRepository.save(b4);
//        };
//    }
//}
