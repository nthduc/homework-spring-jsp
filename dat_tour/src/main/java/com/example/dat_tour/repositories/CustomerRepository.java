package com.example.dat_tour.repositories;

import com.example.dat_tour.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Customer findByEmailOrPhone(String email, String phone);
}
