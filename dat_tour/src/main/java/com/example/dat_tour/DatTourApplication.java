package com.example.dat_tour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatTourApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatTourApplication.class, args);
    }

}
