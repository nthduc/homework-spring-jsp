package com.example.dat_tour.controllers;


import com.example.dat_tour.models.Booking;
import com.example.dat_tour.models.Customer;
import com.example.dat_tour.models.Tour;
import com.example.dat_tour.services.BookingService;
import com.example.dat_tour.services.CustomerService;
import com.example.dat_tour.services.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BookController {
    @Autowired
    private TourService tourService;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private CustomerService customerService;

    @PostMapping("/confirm")
    public String showConfirmation(@RequestParam("bookingId") Long bookingId, Model model) {

        Booking booking = bookingService.getBookingById(bookingId);
        Tour tour = tourService.getTour(booking.getId());
        Customer customer = booking.getCustomer();

        model.addAttribute("booking", booking);
        model.addAttribute("tour", tour);
        model.addAttribute("customer", customer);

        return "confirm";
    }

    @PostMapping("/bookTour")
    public ModelAndView bookTour(@RequestParam("tourId") Long tourId,

            @RequestParam("fullName") String fullName,

            @RequestParam("email") String email,

            @RequestParam("phone") String phone,

            @RequestParam("address") String address,

            @RequestParam("noAdults") Integer noAdults,

            @RequestParam("noChildren") Integer noChildren,

            @RequestParam("departureDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date departureDate) {
        // create customer record
        Customer customer = new Customer();
        customer.setName(fullName);
        customer.setEmail(email);
        customer.setPhone(phone);
        customer.setAddress(address);
        customerService.saveCustomer(customer);

        // create booking record - tạo booking mới nè
        Booking booking = new Booking();
        booking.setCustomer(customer);
        booking.setTour(tourService.getTour(tourId));
        booking.setDepartureDate(departureDate);
        booking.setNoAdults(noAdults);
        booking.setNoChildren(noChildren); // mà sao ở đây set 1,0 sẵn luôn?
        Booking savedBooking = bookingService.bookTour(booking);
        // retrieve booking details from database

        // // pass booking details as model attributes to confirmation page
        ModelAndView modelAndView = new ModelAndView("confirm");
        modelAndView.addObject("tour", savedBooking.getTour());
        modelAndView.addObject("customer", customer);
        modelAndView.addObject("booking", savedBooking);

        // modelAndView.setViewName();
        return modelAndView;
    }
}
