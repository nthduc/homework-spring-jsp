package com.example.dat_tour.controllers;

import com.example.dat_tour.models.Tour;
import com.example.dat_tour.services.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TourController {
    @Autowired
    private TourService tourService;


    @GetMapping(value = {"/tours","/"})
    public String list(Model model) {
        // lấy danh sách tour từ service
        List<Tour> tours = (List<Tour>) tourService.getAllTours();
        // đưa danh sách tour vào model để hiển thị trên view
        model.addAttribute("tours", tours);
        // trả về tên view để hiển thị danh sách tour
        return "listTours";
    }

    @GetMapping("/tour/{id}")
    public String getTourById(@PathVariable Long id, Model model) {
        Tour tour = tourService.getTour(id);
        model.addAttribute("tour", tour);
        return "tourDetails";
    }
    @GetMapping("/bookingTour")
    public String showBookingTour(@RequestParam("tourId") Long tourId, Model model) {
        Tour tour = tourService.getTour(tourId);
        model.addAttribute("tour", tour);
        return "bookingTour";
    }

}

