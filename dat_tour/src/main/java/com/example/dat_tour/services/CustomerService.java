package com.example.dat_tour.services;

import com.example.dat_tour.models.Customer;
import com.example.dat_tour.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer getCustomerByEmailOrPhone(String email, String phone) {
        return customerRepository.findByEmailOrPhone(email, phone);
    }

    public void saveCustomer(Customer customer) {
        customerRepository.save(customer);
    }
}
