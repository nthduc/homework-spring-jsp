package com.example.dat_tour.services;

import com.example.dat_tour.models.Tour;
import com.example.dat_tour.repositories.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourService {
    @Autowired
    private TourRepository tourRepository;

    public Iterable<Tour> getAllTours() {
        return tourRepository.findAll();
    }

    public Tour getTour(long id) {
        return tourRepository.findById(id).orElse(null);
    }

    public Tour saveTour(Tour tour) {
        return tourRepository.save(tour);
    }

    public void deleteTour(long id) {
        tourRepository.deleteById(id);
    }


}
