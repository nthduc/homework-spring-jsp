package com.example.dat_tour.models;

import javax.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Customer customer;
    private Date departureDate;
    private int noAdults;
    private int noChildren;

    @ManyToOne
    private Tour tour;

    public int getTotalSlots() {
        return getNoAdults() + getNoChildren();
    }
}
