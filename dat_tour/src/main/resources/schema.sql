CREATE TABLE tour (
                      id BIGINT AUTO_INCREMENT PRIMARY KEY,
                      description VARCHAR(255) NOT NULL,
                      days VARCHAR(255) NOT NULL,
                      transportation VARCHAR(255) NOT NULL,
                      departure_schedule VARCHAR(255) NOT NULL,
                      price DECIMAL(10,0) NOT NULL
);

CREATE TABLE customer (
                          id BIGINT AUTO_INCREMENT PRIMARY KEY,
                          name VARCHAR(255) NOT NULL,
                          address VARCHAR(255) NOT NULL,
                          email VARCHAR(255) NOT NULL,
                          phone VARCHAR(20) NOT NULL
);

CREATE TABLE booking (
                         id BIGINT AUTO_INCREMENT PRIMARY KEY,
                         customer_id BIGINT NOT NULL,
                         tour_id BIGINT NOT NULL,
                         departure_date DATE NOT NULL,
                         no_adults INT NOT NULL,
                         no_children INT NOT NULL,
                         FOREIGN KEY (customer_id) REFERENCES customer(id),
                         FOREIGN KEY (tour_id) REFERENCES tour(id)
);