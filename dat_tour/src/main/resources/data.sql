INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('PHÚ QUỐC (Khuyến mãi mùa hè)', '3 ngày 2 đêm', 'Bus', 'Hằng Ngày', 1595000);

INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('NHA TRANG', '2 ngày 2 đêm', 'Train', 'Tối thứ 6 và CN', 1540000);

INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('CÔN ĐẢO', '3 ngày 2 đêm', 'Plane', 'Hằng Ngày', 1345000);

INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('PHAN THIẾT-MŨI NÉ', '2 ngày 1 đêm', 'Plane', 'Thứ 7 mỗi tuần', 1250000);

INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('ĐÀ LẠT-ĐỒI MỘNG MƠ', '4 ngày 3 đêm', 'Plane', 'Thứ 7 mỗi tuần', 1320000);

INSERT INTO tour (description, days, transportation, departure_schedule, price)
VALUES ('BUÔN MA THUỘT - GIA LAI - KONTUM', '4 ngày 3 đêm', 'Plane', 'Định kì', 1790000);

INSERT INTO customer (name, address, email, phone)
VALUES ('John Smith', '123 Main St', 'john.smith@example.com', '555-1234');

INSERT INTO customer (name, address, email, phone)
VALUES ('Jane Doe', '456 Oak Ave', 'jane.doe@example.com', '555-5678');

INSERT INTO booking (customer_id, tour_id, departure_date, no_adults, no_children)
VALUES (1, 1, '2023-05-01', 2, 1);

INSERT INTO booking (customer_id, tour_id, departure_date, no_adults, no_children)
VALUES (2, 2, '2023-06-01', 1, 2);