<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>

        <html>

        <head>
            <title>Booking Confirmation</title>
            <style>
                body {
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                    height: 100vh;
                }
            </style>
        </head>

        <body>
            <h1>${date}</h1>
            <h1>Booking Confirmation</h1>
            <h2>Tour Details</h2>
            <ul>
                <li>Program: ${tour.description}</li>
                <li>Duration: ${tour.days}</li>
                <li>Departure Schedule: ${tour.departureSchedule}
            </ul>
            <h2>Customer Information</h2>
            <ul>
                <li>Name: ${customer.name}</li>
                <li>Email: ${customer.email}</li>
                <li>Phone: ${customer.phone}</li>
            </ul>
            <h2>Booking Information</h2>
            <ul>
                <!-- <li>Departure Date: ${booking.departureDate}</li> -->
                <li>No. of Adults: ${booking.noAdults}</li>
                <li>No. of Children: ${booking.noChildren}</li>
            </ul>
            <form action="/tours">
                <button type="submit">Back to List Tours</button>
            </form>
        </body>

        </html>