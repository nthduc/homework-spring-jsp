<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Tour Details</title>
</head>
<body>
<h1>${tour.description}</h1>
<p>Ngày khởi hành: ${tour.departureSchedule}</p>
<p>Số ngày: ${tour.days}</p>
<p>Phương tiện: ${tour.transportation}</p>
<p>Giá: ${tour.price}</p>

<a href="listTours.jsp">CHƯƠNG TRÌNH TOUR</a>
<a href="bookingTour.jsp?tourId=${tour.id}" class="btn btn-primary">Đặt Tour</a>
</body>
</html>