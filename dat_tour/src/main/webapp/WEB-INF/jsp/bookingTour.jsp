<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
            <html>

            <head>
                <title>Booking Tour</title>
                <style>
                    form {
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        padding: 20px;
                        width: 50%;
                        margin: 0 auto;
                    }

                    label {
                        display: block;
                        margin-bottom: 5px;
                    }

                    input[type="text"],
                    input[type="email"],
                    input[type="tel"],
                    input[type="date"],
                    input[type="number"] {
                        width: 100%;
                        padding: 12px 20px;
                        margin: 8px 0;
                        box-sizing: border-box;
                        border: 2px solid #ccc;
                        border-radius: 4px;
                        resize: vertical;
                    }

                    input[type="text"]:focus,
                    input[type="email"]:focus,
                    input[type="tel"]:focus,
                    input[type="date"]:focus,
                    input[type="number"]:focus {
                        border: 2px solid #4CAF50;
                    }

                    button[type="submit"],
                    a.btn {
                        background-color: #4CAF50;
                        color: white;
                        padding: 12px 20px;
                        border: none;
                        border-radius: 4px;
                        cursor: pointer;
                        margin-right: 5px;
                    }

                    button[type="submit"]:hover,
                    a.btn:hover {
                        background-color: #3e8e41;
                    }
                </style>
            </head>

            <body>

                <form method="post" action="/bookTour">
                    <input type="hidden" name="tourId" value="${tour.id}">

                    <span><b>Thông tin khách hàng</b></span>
                    <label for="fullName">Họ tên: (*)</label>
                    <input type="text" id="fullName" name="fullName" required>

                    <label for="address">Địa chỉ:</label>
                    <input type="text" id="address" name="address">

                    <label for="email">Email:(*)</label>
                    <input type="email" id="email" name="email" required>

                    <label for="phone">Điện thoại:</label>
                    <input type="tel" id="phone" name="phone">
                    <span><b>Thông tin Chuyến đi</b></span>
                    <label for="departureDate">Ngày khởi hành:(*)</label>
                    <input type="date" id="departureDate" name="departureDate" required>

                    <label for="noAdults">Số người lớn: (*)</label>
                    <input type="number" id="noAdults" name="noAdults" required min="0">

                    <label for="noChildren">Số trẻ em:</label>
                    <input type="number" id="noChildren" name="noChildren" required min="0">

                    <button type="submit" class="btn btn-primary">Gửi</button>
                    <a href="/tours" class="btn btn-default">Hủy</a>
                </form>
            </body>

            </html>