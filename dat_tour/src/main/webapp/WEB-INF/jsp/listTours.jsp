<%@ page import="java.text.DecimalFormat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 8px;
    }

    th {
        background-color: #4CAF50;
        color: white;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    a.btn {
        display: inline-block;
        background-color: #4CAF50;
        color: white;
        padding: 8px 16px;
        text-align: center;
        text-decoration: none;
        border-radius: 5px;
    }

    a.btn:hover {
        background-color: #3e8e41;
    }
</style>
<!-- Tạo một đối tượng DecimalFormat sử dụng để định dạng giá thành chuỗi -->
<%
    DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
%>
<table>
    <thead>
    <tr>
        <th>Chương trình tour</th>
        <th>Ngày khởi hành</th>
        <th>Giá</th>
        <th>Đặt</th>
    </tr>
    </thead>
    <tbody>

    <%--@elvariable id="tours" type="java.util.List"--%>
    <c:forEach items="${tours}" var="tour">
        <tr>
            <td><a href="/tour/${tour.id}">${tour.description}</a></td>
            <td>${tour.departureSchedule}</td>
            <td>${tour.price}</td>
            <td>
                <a href="bookingTour?tourId=${tour.id}" class="btn btn-primary">Đặt Tour</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>