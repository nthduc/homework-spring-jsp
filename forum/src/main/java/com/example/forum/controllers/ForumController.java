package com.example.forum.controllers;

import com.example.forum.dto.ReplyMessage;
import com.example.forum.models.Topic;
import com.example.forum.models.User;
import com.example.forum.services.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ForumController {
    @Autowired
    ForumService forumService;

    @GetMapping("/")
    public String index(){
        return "login";
    }

    @PostMapping("/login")
    public String login(
            @RequestParam String username,
            @RequestParam String password,
            Model model,
            HttpSession session
    ){
        User user = forumService.checkUser(username, password);
        if(user!= null){
            List<ReplyMessage> topics = forumService.getListTopics();
            model.addAttribute("topics",topics);
            session.setAttribute("user",user);
            return "listTopic";
        }else{
            model.addAttribute("msg","Sai tên đăng nhập hoặc mật khẩu");
            return "login";
        }
    }

    @GetMapping("/logout")
    public String logout(
            HttpSession session
    ){
        session.removeAttribute("user");
        return "login";
    }
    @GetMapping("/listTopic")
    public String listTopic(
            Model model
    ){
        List<ReplyMessage> topics = forumService.getListTopics();
        model.addAttribute("topics",topics);
        return "listTopic";
    }


    @GetMapping("/topics/{id}")
    public String getTopic(
            @PathVariable int id,
            Model model
    ) {
        Topic topic = forumService.getTopicById(id);
        if(topic!=null){
            model.addAttribute("topic",topic);
            return "showTopic";
        }else {
            return "login";
        }
    }
    @GetMapping("/newTopic")
    public String getTopic(

    ) {
        return "newTopic";
    }

    @PostMapping("/topics")
    public String addTopic(
            @RequestParam String title,
            @RequestParam String content,
            Model model,
            HttpSession session
    ){
        forumService.addTopic(title,content, (User) session.getAttribute("user"));
        List<ReplyMessage> topics = forumService.getListTopics();
        model.addAttribute("topics",topics);
        return "listTopic";
    }
}
