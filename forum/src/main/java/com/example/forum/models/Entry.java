package com.example.forum.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Entry {
    private String title;
    private String content;
    private Calendar createdTime;
    private User creator;

    public Entry(){}

    public Entry(String title, String content, Calendar createdTime, User creator){
        this.title = title;
        this.content = content;
        this.createdTime = createdTime;
        this.creator = creator;
    }

    public Entry(String title, String content, User creator) {
        this.title = title;
        this.content = content;
        this.creator = creator;
        createdTime = Calendar.getInstance();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Calendar getCreatedTime() {
        return createdTime;
    }



    public String getCreatedTimeString() {
        Date date = createdTime.getTime();
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
        return dateFormat.format(date);
    }

    public void setCreatedTime(Calendar createdTime) {
        this.createdTime = createdTime;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

}
