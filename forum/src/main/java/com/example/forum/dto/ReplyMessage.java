package com.example.forum.dto;



import java.text.SimpleDateFormat;
import java.util.Calendar;


public class ReplyMessage {
    private long id;
    private String title;
    private String username;
    private String createdTime;
    private int numberReply;

    public ReplyMessage(){}

    public ReplyMessage(Long id, String title, String username, String createdTime, int numberReply) {
        this.id = id;
        this.title = title;
        this.username = username;
        this.createdTime = createdTime;
        this.numberReply = numberReply;
    }

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Calendar createdTime) {
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String formatDate = format.format(createdTime.getTime());
        this.createdTime = formatDate;
    }

    public int getNumberReply(){
        return numberReply;
    }

    public void setNumberReply(int numberReply) {
        this.numberReply = numberReply;
    }
}
