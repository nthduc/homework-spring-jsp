package com.example.forum.services;

import com.example.forum.dto.ReplyMessage;
import com.example.forum.models.Category;
import com.example.forum.models.Message;
import com.example.forum.models.Topic;
import com.example.forum.models.User;
import org.springframework.stereotype.Service;


import java.util.*;
@Service
public class ForumService {
    private static final ForumService instance = new ForumService();
    private Map<String, User> users;
    private Map<Integer, Topic> topics;

    public ForumService() {
        init();
    }

    private void init() {
        users = new HashMap<String, User>();
        topics = new HashMap<Integer, Topic>();
        Category category1 = new Category("Học hành");

        users.put("chuotcon", new User("chuotcon", "chuotcon", "chuotcon@yahoo.com",
                new GregorianCalendar(2008, Calendar.FEBRUARY, 22).getTime()));
        users.put("leb",
                new User("leb", "leb", "leb@yahoo.com", new GregorianCalendar(2007, Calendar.APRIL, 15).getTime()));
        users.put("doctorQ", new User("doctorQ", "doctorQ", "doctorQ@yahoo.com",
                new GregorianCalendar(2008, Calendar.FEBRUARY, 22).getTime()));
        users.put("itss", new User("itss", "itss", "itss@yahoo.com",
                new GregorianCalendar(2007, Calendar.AUGUST, 30).getTime()));
        users.put("dt03", new User("dt03", "dt03", "dt03@hotmail.com",
                new GregorianCalendar(2007, Calendar.JANUARY, 7).getTime()));
        users.put("GSKH",
                new User("GSKH", "GSKH", "GSKH@yahoo.com", new GregorianCalendar(2007, Calendar.JULY, 14).getTime()));

        Topic topic1 = new Topic(1, "Chuyện học phí!!!",
                "Từ lớp mẫu giáo đến lớp 12-trường công-Tại sao lại phải đóng học phí?!", users.get("doctorQ"),
                category1);
        topic1.addMessage(new Message("Re: Chuyện học phí!!!",
                "Thưa anh (chị) em muôn hỏi là sinh viên dân tộc thiểu số có được miễn giảm "
                        + "học phí hay không? Và những yêu cầu gì đối với sinh viên "
                        + "dân tôc thiểu số để được miễn giảm học phí? Em cảm ơn anh (chị)\nNgoc Linh",
                users.get("chuotcon")));

        Topic topic2 = new Topic(2, "Thủ tướng: Trường ĐH tự quyết mức học phí",
                "Không như cấp bật tiểu học và trung học...",
                users.get("leb"), category1);
        Topic topic3 = new Topic(3, "Học phí trái buổi",
                "Tiền học phí trái buổi quá cao so với mặt bằng chung.",
                users.get("itss"), category1);
        Topic topic4 = new Topic(4, "Một số mốc quan trọng trong đề án \"Tăng học phí\"",
                "Nhằm tăng thêm kinh phí và ...",
                users.get("GSKH"), category1);
        topic4.addMessage(new Message("Re: Một số mốc quan trọng trong đề án \"Tăng học phí\"", "Cần thảo luận lại.",
                users.get("dt03")));
        Topic topic5 = new Topic(5, "Xung quanh dự thảo của Đề án tăng học phí",
                "Trong năm 2022....",
                users.get("leb"), category1);
        topic5.addMessage(new Message("Re: Xung quanh dự thảo của Đề án tăng học phí", "Chất lượng sẽ tăng theo.",
                users.get("GSKH")));

        topics.put(1, topic1);
        topics.put(2, topic2);
        topics.put(3, topic3);
        topics.put(4, topic4);
        topics.put(5, topic5);

    }

    public Collection<Topic> getTopics() {
        return topics.values();
    }

    public User checkUser(String name, String password) {
        User user = users.get(name);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public Topic findTopic(Integer id) {
        return topics.get(id);
    }

    public static ForumService getInstance() {
        return instance;
    }

    public List<ReplyMessage> getListTopics() {
        List<ReplyMessage> result = new ArrayList<>();
        topics.forEach((k, v) -> {
            ReplyMessage topicResponse = new ReplyMessage();
            topicResponse.setId(Long.valueOf(k));
            topicResponse.setTitle(v.getTitle());
            topicResponse.setNumberReply(v.getMessages().size());
            topicResponse.setCreatedTime(v.getNewMessage().getCreatedTime());
            topicResponse.setUsername(v.getNewMessage().getCreator().getUsername());

            result.add(topicResponse);
        });
        return result;
    }

    public Topic getTopicById(int id) {
        return topics.get(id);
    }

    public void addTopic(String title, String content, User user) {
        Category cat1 = new Category("Học hành");
        Topic topic1 = new Topic(topics.size() + 1, title,
                content, users.get(user.getUsername()),
                cat1);
        topics.put(topics.size() + 1, topic1);
    }
}
