<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <title>Danh sách bài viết</title>
</head>
<body>
<div id="listTopic">
    <div id="header">
        <p>Chào ${user.username} | <a href="/logout">Thoát</a></p>
    </div>
    <div id="content">
        <h2>Diễn đàn: Chuyện học phí và các chính sách hỗ trợ học tập</h2>
        <button>
            <a href="/newTopic" style="text-decoration: none;color: black">
                Gửi bài mới
            </a>
        </button>
        <table>
            <tr style="background-color: #ccc;">
                <th style="width: 700px;">Chủ đề</th>
                <th style="width: 100px;">Hồi âm</th>
            </tr>
            <c:forEach items="${topics}" var="topic">
                <tr class="table-row">
                    <td>
                        <a class="topic-title" href="/topics/${topic.id}">${topic.title}</a>
                        <p class="topic-context">Bài mới nhất by <a href="">${topic.username}</a>, ${topic.createdTime}</p>
                    </td>
                    <td style="text-align: center;">
                        ${topic.numberReply}
                    </td>
                </tr>
            </c:forEach>

        </table>
    </div>
</div>
</body>
</html>
