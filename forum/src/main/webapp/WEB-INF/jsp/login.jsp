<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <title>Đăng Nhập</title>
</head>
<body>
<div id="login">
    <div class="container">
        <h3 class="title">Đăng nhập</h3>
        <form action="/login" method="post">
            <table>
                <tr>
                    <td style="text-align: right;"><label for="username">Tên đăng nhập:</label></td>
                    <td><input type="text" name="username" id="username"></td>
                </tr>
                <tr>
                    <td style="text-align: right;"><label for="password">Mật khẩu:</label></td>
                    <td><input type="password" name="password" id="password"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Đăng nhập"></td>
                </tr>
            </table>
        </form>
        <p style="margin: 0;text-align: center">${msg}</p>
    </div>
</div>
</body>
</html>
