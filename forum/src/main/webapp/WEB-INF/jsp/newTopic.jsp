<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <title>Bài viết mới</title>
</head>
<body>
<div>
    <div id="header">
        <p>Chào ${user.username} | <a href="/logout">Thoát</a></p>
    </div>
    <form method="post" action="/topics">
        <div style="height: 320px; width: 100%; margin-left: 300px;margin-top: 64px">
            <h2 style="margin-bottom: 0px;">Tiêu đề</h2>
            <input name="title" type="text" size="60" />
            <h2 style="margin-bottom: 0px;">Nội dung</h2>
            <textarea name="content"  rows="10" cols="95"></textarea>
        </div>
        <div style="margin-left: 300px;">
            <input id="button" type="submit" value="Gửi"/>
            <button type="button">
                <a href="/listTopic" style="text-decoration: none;color:black">Hủy bỏ</a>
            </button>
        </div>
    </form>
</div>

</body>
</html>
