<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <title>Show Bài viết</title>
</head>

<body>
<div id="showTopic">
    <div id="header">
        <p>Chào ${user.username} | <a href="/logout">Thoát</a></p>
    </div>
    <div id="content">
        <h2 style="margin: 0;">Chủ đề: ${topic.title}</h2>
        <p style="margin-top: 0;">Bài mới nhất gửi ${topic.getNewMessage().getCreatedTimeString()}, do <b>${topic.getNewMessage().creator.username}</b> gửi. ${topic.getMessages().size()} hồi âm</p>

        <div class="list-topic">
            <div class="topic-item">
                <p class="created-time">${topic.getCreatedTimeString()}</p>
                <div class="topic-body">
                    <div class="topic-creator">
                        <p><b>${topic.creator.username}</b></p>
                        <p>Tham gia ${topic.creator.joinDate}</p>
                    </div>
                    <div class="topic-content">
                        <div class="header">
                            <p><b>${topic.title}</b></p>
                            <a href="">Trả lời</a>
                        </div>
                        <div class="body">
                            <p>${topic.content}</p>
                        </div>
                    </div>
                </div>
            </div>
            <c:forEach items="${topic.messages}" var="message">
                <div class="topic-item">
                    <p class="created-time">${message.getCreatedTimeString()}</p>
                    <div class="topic-body">
                        <div class="topic-creator">
                            <p><b>${message.creator.username}</b></p>
                            <p>Tham gia ${message.creator.joinDate}</p>
                        </div>
                        <div class="topic-content">
                            <div class="header">
                                <p><b>${message.title}</b></p>
                                <a href="">Trả lời</a>
                            </div>
                            <div class="body">
                                <p>${message.content}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>

        </div>

        <div class="link-wrapper">
            <a class="link-list-topic" href="/listTopic">Danh sách chủ đề</a>
        </div>
    </div>
</div>
</body>

</html>
